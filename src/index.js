const $ = require( "jquery" );
const { formatDistance } = require('date-fns');
const { pl } = require('date-fns/locale')
const FlvPlayer = require("flvplayer");
import("flvplayer/dist/flvplayer-control.js");
//import("flvplayer/dist/flvplayer-decoder-multiple.js");

const superagent = require('superagent');


class VideoCalendar extends HTMLElement {
  // Can define constructor arguments if you wish.
  constructor() {
    super();
    this.flvPlayer = null;
    this.calendarAPIurl="https://europe-west1-video-piaski.cloudfunctions.net/calendarStatus";
    this.flvplayerDecoderURL="https://youngtree.org/video/flvplayer-decoder-multiple.js";
    this.refreshStatusTimeOffset = 1 * 60 *1000; //
    this.startAheadTime = 2*60*1000; //pokaz transmisje 2 minuty przed wydarzeniem
  }


 static get observedAttributes() {
    return ["calendarId", "videoUrl"];
  }


 attributeChangesCallback(key, oldVal, newVal) {
 	this.setAttribute(key,newVal);
 }
 // We Can Get And Set Properties
 set calendarId(val) {this.setAttribute('calendarId', val);}
 get calendarId() {this.getAttribute('calendarId');}
 set videoUrl(val) {this.setAttribute('videoUrl', val)}
 get videoUrl() {this.getAttribute('videoUrl')}

 // We Have Lifecycle Hooks
 disconnectedCallBack(){}
 

 connectedCallback() {
    this.innerHTML = `
         <DIV id="__initialDIV">Sprawdzam dostępność transmisji...........</DIV>
         <DIV id="__videoDIV" style="display: none">
  	        <div id="__videoElementDIV"></div>
         </DIV>
         <DIV id="__calendarDIV" style="display: none">
  	        <p>Obecnie nie trwa żadna transmisja.</p>
  	        <div id="_calendarDetailsDIV"></div>
  	     </DIV>
     `
     this.updateStatus() 
  }



  updateStatus() {
  	const url = this.calendarAPIurl;
  	const calendarId = this.getAttribute('calendarId')
  	const startAheadTime = this.startAheadTime
    superagent.get(url)
		.query({calendarId})
		.end((err, res) => {
		    if (err) { return console.log("erro geting calendar status: " + err); }
			console.log(res.text);
	        const event = JSON.parse(res.text)
	        const start = event.start.dateTime || event.start.date;
	        const startDate = new Date(start)
	        const nowTime = new Date().getTime()

	        if (startDate.getTime() < nowTime- startAheadTime) {
	        	this.startStreamIfNotStarted(event)
	        	const timeToEnd = new Date(event.end.dateTime || event.end.date).getTime() - nowTime + this.refreshStatusTimeOffset
	        	this.scheduleUpdate(timeToEnd)
	        } else {
	        	this.showEventDetails(event)
	        	const timeToBegining = 1*1000*5 ;//startDate.getTime() - nowTime - this.refreshStatusTimeOffset
	        	this.scheduleUpdate(timeToBegining)
	        }
     });
  }


  scheduleUpdate(time) {
  	console.log("refresh scheduled in: " + time)
  	setTimeout(function(obj){obj.updateStatus()}, time, this)
  }
	

  showEventDetails(event) {
    $( "#__videoDIV" ).hide("slow");
    $( "#__calendarDIV" ).show("slow");
    $( "#__initialDIV" ).hide("slow");	   


    const distanceInWords_pl = (date) => {
		const options = {includeSeconds: false, locale: pl}
    	return formatDistance(date, new Date(), options)
    }

    const startDate = new Date(event.start.dateTime || event.start.date);
    $( "#_calendarDetailsDIV" ).html(
    	`<h3>Transmisja ' ${event.summary}' rozpocznie się <b> za ${distanceInWords_pl(startDate)} </b></h3>`); 
         console.log( `<h3>Transmisja ' ${event.summary}' rozpocznie się za <b> ${distanceInWords_pl(startDate)}</b></h3>`)
  }


 startStreamIfNotStarted() {
  	   const videoUrl = this.getAttribute('videoUrl')
  	   console.log("Start playing from: "  + videoUrl)
  	   if (this.flvPlayer) {flvPlayer.destroy();}
	   if (FlvPlayer.isSupported()) {
	        this.flvPlayer = new FlvPlayer({
	            url: videoUrl,
	            live: true,
	            autoplay: true,
	            container: "#__videoElementDIV",
	            decoder: this.flvplayerDecoderURL
	        });
	    }

  	if (!$( "#__videoDIV" ).is(":visible")) {
  	    $( "#__calendarDIV" ).hide("slow");
	    $( "#__initialDIV" ).hide("slow");	    
	    $( "#__videoDIV" ).show("slow");
	}
  }

	

}// Register to the Browser from `customElements` API
window.customElements.define("video-calendar", VideoCalendar);
