# GCalendarWidgetForVideoStreaming

A HTML widget showing a video stream as scheduled in Google Calendar.
The widget can be easly installed on any webpage, no server side coding is required.


# Installation

`<video-calendar calendarId="<idOfYourGoogleCalendar>" videoUrl="<your video stream>"/>`


### Google Calendar ID
You may use any GCalendar however we recomend to create a dedicated calendar for the purpose of the media stream management.
The calendar has to be "Make available to public" with  "see all evant details" - this process is just few clicks - https://support.google.com/calendar/answer/37083

Your main Google calendarId is simply your *gmail address*. 
For other calendars you have to copy the calendar id from calenadar settings.
It will have a format similar to qidv73f5p7tbemo9ic8qen65ek@group.calendar.google.com

Please note that upcoming events details may be displayed before the event while ther is no streaming in progress

### Video Stream Url

The video stream in the vidget played by FlvPlayer so you can use any stream configuration url suported by https://www.npmjs.com/package/flvplayer
In particular https and websocket are suported. The sample url may look like: https://video.youngtree.org/cctv/piaski_wielkie.flv

### Sample 

You can always try with the following settings


`<video-calendar calendarId="parafiapiaskiwielkie@gmail.com" videoUrl="https://video.youngtree.org/cctv/piaski_wielkie.flv"/>`



# Localization

Currently the widget is localized to Polish langauge only. Contact me if you like to support other languages

